## Introduction

Translation of LIGO report on gravitational waves to [Lezgi (Lezgian) language](https://en.wikipedia.org/wiki/Lezgian_language).


## Links
* [Original report](https://www.ligo.caltech.edu/page/press-release-gw170104)
* Inspired by the [translation to Siksika (Blackfoot)](https://www.ligo.org/detections/GW170104/press-release/pr-blackfoot.pdf)
